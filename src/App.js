import React, {useState} from 'react';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import Task from './Task/Task'
import './App.css';

const App = () => {
  const [task, setTask]= useState([
    {text:'Buy Milk',id:'task1'},
    {text:'Buy Drink',id:'task2'},
    {text:'Buy Battery',id:'task3'},
    {text:'Buy Fruits',id:'task4'},
  ]);
  const [inputTask, setInputTask]= useState('New Task');

  const addNewTask = () =>{
      if(inputTask){
        const item ={text:inputTask, id:'task22'+ 1 + inputTask};
        setTask((task)=>{
        return[...task, item];
      });
      setInputTask('');
      };
  };

const removeTask = id =>{
  const index = task.findIndex(task => task.id === id);
  const taskCopy = [...task];
  taskCopy.splice(index,1);
  setTask(taskCopy);
};

let taskList;

taskList=(
  <>
  <div className="tasks">
  {task.map((task,index)=>{
    return <Task
    key = {task.id+ index }    
    checked = {!task.completed}
    remove = {()=> removeTask(task.id)}
    text ={index + 1 + '. ' + task.text}
    />
  })}
  </div>
  </>
);

  return (
    <div className="App">
      <AddTaskForm 
        newTask={event=>setInputTask(event.target.value)}
        btnOnClick={addNewTask}
        value={inputTask}
        task = 'New Task'
      />
      {taskList}
    </div>
  );
};

export default App;

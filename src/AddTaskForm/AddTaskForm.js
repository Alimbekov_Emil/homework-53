import React  from 'react';
import './AddTaskForm.css';

const AddTaskForm = props =>{
    return(
        <div className='todo'>
            <b>{props.task}</b>
            <input type='text' value={props.value} onChange={props.newTask}></input>
            <button onClick={props.btnOnClick}>Add</button>
        </div>
    );
};

export default AddTaskForm;
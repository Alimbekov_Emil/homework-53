import React  from 'react';
import './Task.css';

const Task = props =>{
    return(
        <div className='task'>
            <b>{props.text}</b>
            <button onClick={props.remove}>Delete</button>
        </div>
    );
};

export default Task;